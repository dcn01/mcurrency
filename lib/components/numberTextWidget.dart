import 'package:flutter/material.dart';
import 'package:mcurrency/utils/constants.dart';

class NumberTextWidget extends StatelessWidget {
  NumberTextWidget({@required this.label, this.onNumberPressed});

  final String label;
  final Function onNumberPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onNumberPressed,
      child: Text(
        label,
        textAlign: TextAlign.center,
        style: kNumberTextStyle,
      ),
    );
  }
}
