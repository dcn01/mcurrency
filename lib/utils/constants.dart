import 'package:flutter/material.dart';

const baseUrl = 'https://forex.cbm.gov.mm/api/latest';

const fontRegular = 'MeriendaRegular';
const fontBold = 'MeriendaBold';

const kToolbarTextStyle =
    TextStyle(color: Colors.white, fontFamily: fontRegular);

const kCountryTextStyle =
    TextStyle(fontSize: 25.0, color: Colors.white, fontFamily: fontRegular);

const kNumberTextStyle = TextStyle(
  fontSize: 30.0,
  color: Colors.white,
  fontFamily: fontBold,
);

const kNumberLabelTextStyle = TextStyle(
  fontSize: 18.0,
  color: Colors.white,
  fontFamily: fontRegular,
);

const kCurrencyConvertTextStyle = TextStyle(
  fontSize: 14.0,
  color: Colors.lightBlue,
  fontFamily: fontRegular,
);
