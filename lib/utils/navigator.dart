import 'package:flutter/material.dart';
import 'package:mcurrency/pages/aboutPage.dart';
import 'package:mcurrency/pages/currencyPage.dart';

goToCurrencyPage(BuildContext context) {
  _pushWidgetWithFade(context, CurrencyPage());
}

goToAboutPage(BuildContext context) {
  _pushWidgetWithFade(context, AboutPage());
}

_pushWidgetWithFade(BuildContext context, Widget widget) {
  Navigator.of(context).push(PageRouteBuilder(
    transitionsBuilder: (context, animation, secondaryAnimation, child) =>
        FadeTransition(opacity: animation, child: child),
    pageBuilder: (BuildContext context, Animation animation,
        Animation secondaryAnimation) {
      return widget;
    },
  ));
}
