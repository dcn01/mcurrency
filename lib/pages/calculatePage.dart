import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mcurrency/components/numberLabelTextWidget.dart';
import 'package:mcurrency/components/numberTextWidget.dart';
import 'package:mcurrency/components/waveLayerWidget.dart';
import 'package:mcurrency/model/countryCurrency.dart';
import 'package:mcurrency/model/currencyItem.dart';
import 'package:mcurrency/utils/constants.dart';
import 'package:mcurrency/utils/navigator.dart';
import 'package:progress_hud/progress_hud.dart';

class CalculatePage extends StatefulWidget {
  CalculatePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _CalculatePageState createState() => _CalculatePageState();
}

class _CalculatePageState extends State<CalculatePage> {
  int _numberValue = 0;
  String _result = '0';
  String _myanmarKyatResult = '0';
  List<CurrencyItem> currencyItemList = List();
  CountryCurrency countryCurrency;
  int currencyResult = 1530;
  String currencyCountry = 'usd';
  String changeFormat;
  String changeCurrency = 'USD';
  ProgressHUD _progressHUD;
  bool _loading = true;

  String _getTotalResult() {
    if (_result.length <= 6 && _result.length > 0) {
      return _result;
    } else {
      _numberValue = 0;
      _result = '0';
      return _result;
    }
  }

  String _calculateCurrency() {
    int calculateResult = int.parse(_getTotalResult()) * currencyResult;
    _myanmarKyatResult = calculateResult.toString();
    return _myanmarKyatResult;
  }

  void addNumberValue(int number) {
    setState(() {
      if (_result == '0') {
        _result = number.toString();
      } else {
        _result += number.toString();
      }
    });
  }

  void restartResult() {
    setState(() {
      _numberValue = 0;
      _result = '0';
      _myanmarKyatResult = '0';
    });
  }

  void _showBottomSheet() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return ListView(
            padding: EdgeInsets.all(6.0),
            scrollDirection: Axis.vertical,
            children: <Widget>[
              GestureDetector(
                onTap: () => selectedCountry('usd'),
                child: ListTile(
                  leading: Image.asset(
                    'assets/images/usd.png',
                    width: 30.0,
                    height: 30.0,
                  ),
                  title: Text('United Stated Dollar'),
                ),
              ),
              Divider(
                color: Colors.grey,
                height: 1.0,
              ),
              GestureDetector(
                onTap: () => selectedCountry('eur'),
                child: ListTile(
                  leading: Image.asset(
                    'assets/images/eur.png',
                    width: 30.0,
                    height: 30.0,
                  ),
                  title: Text('Euro'),
                ),
              ),
              Divider(
                color: Colors.grey,
                height: 1.0,
              ),
              GestureDetector(
                onTap: () => selectedCountry('cny'),
                child: ListTile(
                  leading: Image.asset(
                    'assets/images/cny.png',
                    width: 30.0,
                    height: 30.0,
                  ),
                  title: Text('Chinese Yuan'),
                ),
              ),
              Divider(
                color: Colors.grey,
                height: 1.0,
              ),
              GestureDetector(
                onTap: () => selectedCountry('sgd'),
                child: ListTile(
                  leading: Image.asset(
                    'assets/images/sgd.png',
                    width: 30.0,
                    height: 30.0,
                  ),
                  title: Text('Singapore Dollar'),
                ),
              ),
              Divider(
                color: Colors.grey,
                height: 1.0,
              ),
              GestureDetector(
                onTap: () => selectedCountry('krw'),
                child: ListTile(
                  leading: Image.asset(
                    'assets/images/krw.png',
                    width: 30.0,
                    height: 30.0,
                  ),
                  title: Text('Korean Woun'),
                ),
              ),
              Divider(
                color: Colors.grey,
                height: 1.0,
              ),
              GestureDetector(
                onTap: () => selectedCountry('thb'),
                child: ListTile(
                  leading: Image.asset(
                    'assets/images/thb.png',
                    width: 30.0,
                    height: 30.0,
                  ),
                  title: Text('Thai Baht'),
                ),
              ),
              Divider(
                color: Colors.grey,
                height: 1.0,
              ),
              GestureDetector(
                onTap: () => selectedCountry('vnd'),
                child: ListTile(
                  leading: Image.asset(
                    'assets/images/vnd.png',
                    width: 30.0,
                    height: 30.0,
                  ),
                  title: Text('Vietnamese Dong'),
                ),
              ),
              Divider(
                color: Colors.grey,
                height: 1.0,
              ),
              GestureDetector(
                onTap: () => selectedCountry('inr'),
                child: ListTile(
                  leading: Image.asset(
                    'assets/images/inr.png',
                    width: 30.0,
                    height: 30.0,
                  ),
                  title: Text('Indian Rupee'),
                ),
              ),
              Divider(
                color: Colors.grey,
                height: 1.0,
              ),
              GestureDetector(
                onTap: () => selectedCountry('jpy'),
                child: ListTile(
                  leading: Image.asset(
                    'assets/images/jpy.png',
                    width: 30.0,
                    height: 30.0,
                  ),
                  title: Text('Japanese Yen'),
                ),
              ),
              Divider(
                color: Colors.grey,
                height: 1.0,
              ),
              GestureDetector(
                onTap: () => selectedCountry('myr'),
                child: ListTile(
                  leading: Image.asset(
                    'assets/images/myr.png',
                    width: 30.0,
                    height: 30.0,
                  ),
                  title: Text('Malaysian Ringgit'),
                ),
              ),
            ],
          );
        });
  }

  void selectedCountry(String currency) {
    currencyCountry = currency;
    getCurrencyPrice(currency);
    Navigator.pop(context);
  }

  @override
  void initState() {
    super.initState();
    _progressHUD = new ProgressHUD(
      color: Colors.white,
      containerColor: Colors.blue,
      borderRadius: 5.0,
      text: 'Loading...',
    );
    callCurrencyApi();
  }

  void callCurrencyApi() async {
    final urlResponse = await http.get(baseUrl);
    if (urlResponse.statusCode == 200) {
      countryCurrency = CountryCurrency.fromJson(jsonDecode(urlResponse.body));
      TempStore().currencyList = countryCurrency.getCurrenciesItem();
      getCurrencyPrice('usd');
      currencyItemList = TempStore().currencyList;
      dismissProgressHUD();
    } else {
      dismissProgressHUD();
    }
  }

  void getCurrencyPrice(String currency) {
    switch (currency) {
      case 'usd':
        updateUiAndValue('USD', countryCurrency.usd.replaceAll(',', ''));
        break;
      case 'eur':
        updateUiAndValue('EURO', countryCurrency.eur.replaceAll(',', ''));
        break;
      case 'cny':
        updateUiAndValue('YUAN', countryCurrency.cny.replaceAll(',', ''));
        break;
      case 'sgd':
        updateUiAndValue('DOLLAR', countryCurrency.sgd.replaceAll(',', ''));
        break;
      case 'krw':
        updateUiAndValue('WOUN', countryCurrency.krw.replaceAll(',', ''));
        break;
      case 'thb':
        updateUiAndValue('BAHT', countryCurrency.thb.replaceAll(',', ''));
        break;
      case 'vnd':
        updateUiAndValue('DONG', countryCurrency.vnd.replaceAll(',', ''));
        break;
      case 'inr':
        updateUiAndValue('RUPEE', countryCurrency.inr.replaceAll(',', ''));
        break;
      case 'jpy':
        updateUiAndValue('YEN', countryCurrency.jpy.replaceAll(',', ''));
        break;
      case 'myr':
        updateUiAndValue('RINGGIT', countryCurrency.myr.replaceAll(',', ''));
        break;
    }
    double getResult = double.parse(changeFormat);
    currencyResult = getResult.toInt();
  }

  void updateUiAndValue(String currency, String price) {
    setState(() {
      changeCurrency = currency;
      changeFormat = price;
    });
  }

  void dismissProgressHUD() {
    setState(() {
      if (_loading) {
        _progressHUD.state.dismiss();
      } else {
        _progressHUD.state.show();
      }

      _loading = !_loading;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: TextStyle(color: Colors.white),
        ),
        automaticallyImplyLeading: false,
        elevation: 1.0,
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.present_to_all,
                color: Colors.white,
              ),
              onPressed: () =>
                  currencyItemList.isEmpty ? null : goToCurrencyPage(context)),
          IconButton(
            icon: Icon(
              Icons.info_outline,
              color: Colors.white,
            ),
            onPressed: () => goToAboutPage(context),
          )
        ],
      ),
      body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  stops: [
                0.1,
                0.5,
                0.7,
                0.9
              ],
                  colors: [
                Colors.lightBlue[300],
                Colors.lightBlue[200],
                Colors.lightBlue[200],
                Colors.lightBlue[300],
              ])),
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      _showBottomSheet();
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          top: 25.0, bottom: 12.0, left: 12.0, right: 12.0),
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Text(
                            changeCurrency,
                            style: kCountryTextStyle,
                          ),
                          Row(
                            children: <Widget>[
                              Text(
                                _getTotalResult(),
                                style: kCountryTextStyle,
                              ),
                              SizedBox(
                                width: 4.0,
                              ),
                              Text(
                                '\$',
                                style: kCountryTextStyle,
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Divider(
                      color: Colors.white,
                      height: 1.0,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(12.0),
                    width: double.infinity,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Text(
                          'MYANMAR',
                          style: kCountryTextStyle,
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              _calculateCurrency(),
                              style: kCountryTextStyle,
                            ),
                            SizedBox(
                              width: 4.0,
                            ),
                            Text(
                              'K',
                              style: kCountryTextStyle,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  WaveLayerWidget(),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                                child: NumberTextWidget(
                              label: '1',
                              onNumberPressed: () {
                                _numberValue = 1;
                                addNumberValue(_numberValue);
                              },
                            )),
                            Expanded(
                                child: NumberTextWidget(
                              label: '2',
                              onNumberPressed: () {
                                _numberValue = 2;
                                addNumberValue(_numberValue);
                              },
                            )),
                            Expanded(
                                child: NumberTextWidget(
                              label: '3',
                              onNumberPressed: () {
                                _numberValue = 3;
                                addNumberValue(_numberValue);
                              },
                            )),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                                child: NumberTextWidget(
                              label: '4',
                              onNumberPressed: () {
                                _numberValue = 4;
                                addNumberValue(_numberValue);
                              },
                            )),
                            Expanded(
                                child: NumberTextWidget(
                              label: '5',
                              onNumberPressed: () {
                                _numberValue = 5;
                                addNumberValue(_numberValue);
                              },
                            )),
                            Expanded(
                                child: NumberTextWidget(
                              label: '6',
                              onNumberPressed: () {
                                _numberValue = 6;
                                addNumberValue(_numberValue);
                              },
                            )),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                                child: NumberTextWidget(
                              label: '7',
                              onNumberPressed: () {
                                _numberValue = 7;
                                addNumberValue(_numberValue);
                              },
                            )),
                            Expanded(
                                child: NumberTextWidget(
                              label: '8',
                              onNumberPressed: () {
                                _numberValue = 8;
                                addNumberValue(_numberValue);
                              },
                            )),
                            Expanded(
                                child: NumberTextWidget(
                              label: '9',
                              onNumberPressed: () {
                                _numberValue = 9;
                                addNumberValue(_numberValue);
                              },
                            )),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                                child: NumberLabelTextWidget(
                              label: 'assets/images/clear_left.png',
                              onClear: () {
                                _numberValue = 0;
                                restartResult();
                              },
                            )),
                            Expanded(
                                child: NumberTextWidget(
                              label: '0',
                              onNumberPressed: () {
                                _numberValue = 0;
                                addNumberValue(_numberValue);
                              },
                            )),
                            Expanded(
                                child: NumberLabelTextWidget(
                              label: 'assets/images/clear_right.png',
                              onClear: () {
                                _numberValue = 0;
                                restartResult();
                              },
                            )),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
              _progressHUD,
            ],
          )),
    );
  }
}
