import 'package:flutter/material.dart';
import 'package:mcurrency/utils/constants.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About mCurrency'),
      ),
      body: Container(
        margin: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Container(
              alignment: Alignment.center,
              child: Container(
                width: 110.0,
                height: 110.0,
                child: Image.asset('assets/images/mCurrencyLogo.png'),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Meet mCurrency\n\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy'
              'text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived'
              'with the release of Letraset sheets containing Lorem Ipsum passages, including versions of Lorem Ipsum.',
              style: TextStyle(fontFamily: fontRegular),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              'Brought to you by Devshub',
              style:
                  TextStyle(color: Colors.blueAccent, fontFamily: fontRegular),
            ),
            SizedBox(
              height: 8.0,
            ),
            Text(
              'Version 1.0',
              style: TextStyle(color: Colors.grey, fontFamily: fontRegular),
            )
          ],
        ),
      ),
    );
  }
}
