import 'package:mcurrency/model/currencyItem.dart';

class CountryCurrency {
  String usd;
  String zar;
  String npr;
  String cny;
  String chf;
  String thb;
  String pkr;
  String kes;
  String egp;
  String bdt;
  String sar;
  String lak;
  String idr;
  String khr;
  String sgd;
  String lkr;
  String nzd;
  String czk;
  String jpy;
  String vnd;
  String php;
  String krw;
  String hkd;
  String brl;
  String myr;
  String rsd;
  String cad;
  String gbp;
  String sek;
  String nok;
  String ils;
  String aud;
  String dkk;
  String rub;
  String kwd;
  String inr;
  String bnd;
  String eur;

  CountryCurrency(
      {this.usd,
      this.zar,
      this.npr,
      this.cny,
      this.chf,
      this.thb,
      this.pkr,
      this.kes,
      this.egp,
      this.bdt,
      this.sar,
      this.lak,
      this.idr,
      this.khr,
      this.sgd,
      this.lkr,
      this.nzd,
      this.czk,
      this.jpy,
      this.vnd,
      this.php,
      this.krw,
      this.hkd,
      this.brl,
      this.myr,
      this.rsd,
      this.cad,
      this.gbp,
      this.sek,
      this.nok,
      this.ils,
      this.aud,
      this.dkk,
      this.rub,
      this.kwd,
      this.inr,
      this.bnd,
      this.eur});

  factory CountryCurrency.fromJson(Map<String, dynamic> json) {
    return CountryCurrency(
        usd: json['rates']['USD'],
        zar: json['rates']['ZAR'],
        npr: json['rates']['NPR'],
        cny: json['rates']['CNY'],
        chf: json['rates']['CHF'],
        thb: json['rates']['THB'],
        pkr: json['rates']['PKR'],
        kes: json['rates']['KES'],
        egp: json['rates']['EGP'],
        bdt: json['rates']['BDT'],
        sar: json['rates']['SAR'],
        lak: json['rates']['LAK'],
        idr: json['rates']['IDR'],
        khr: json['rates']['KHR'],
        sgd: json['rates']['SGD'],
        lkr: json['rates']['LKR'],
        nzd: json['rates']['NZD'],
        czk: json['rates']['CZK'],
        jpy: json['rates']['JPY'],
        vnd: json['rates']['VND'],
        php: json['rates']['PHP'],
        krw: json['rates']['KRW'],
        hkd: json['rates']['HKD'],
        brl: json['rates']['BRL'],
        myr: json['rates']['MYR'],
        rsd: json['rates']['RSD'],
        cad: json['rates']['CAD'],
        gbp: json['rates']['GBP'],
        sek: json['rates']['SEK'],
        nok: json['rates']['NOK'],
        ils: json['rates']['ILS'],
        aud: json['rates']['AUD'],
        dkk: json['rates']['DKK'],
        rub: json['rates']['RUB'],
        kwd: json['rates']['KWD'],
        inr: json['rates']['INR'],
        bnd: json['rates']['BND'],
        eur: json['rates']['EUR']);
  }

  List<CurrencyItem> getCurrenciesItem() {
    List<CurrencyItem> currencyList = List();

    currencyList.add(CurrencyItem('assets/images/eur.png', 'EUR', eur, 'Euro'));
    currencyList.add(CurrencyItem(
        'assets/images/usd.png', 'USD', usd, 'United State Dollar'));
    currencyList
        .add(CurrencyItem('assets/images/cny.png', 'CNY', cny, 'Chinese Yuan'));
    currencyList.add(
        CurrencyItem('assets/images/sgd.png', 'SGD', sgd, 'Singapore Dollar'));
    currencyList
        .add(CurrencyItem('assets/images/krw.png', 'KRW', krw, 'Korean Won'));
    currencyList
        .add(CurrencyItem('assets/images/thb.png', 'THB', thb, 'Thai Baht'));
    currencyList.add(
        CurrencyItem('assets/images/vnd.png', 'VND', vnd, 'Vietnamese Dong'));
    currencyList
        .add(CurrencyItem('assets/images/inr.png', 'INR', inr, 'Indian Rupee'));
    currencyList
        .add(CurrencyItem('assets/images/jpy.png', 'JPY', jpy, 'Japanese Yen'));
    currencyList.add(
        CurrencyItem('assets/images/bdt.png', 'BDT', bdt, 'Bangladesh Taka'));
    currencyList.add(
        CurrencyItem('assets/images/bnd.png', 'BND', bnd, 'Brunei Dollar'));
    currencyList.add(
        CurrencyItem('assets/images/idr.png', 'IDR', idr, 'Indonesian Rupiah'));
    currencyList.add(
        CurrencyItem('assets/images/php.png', 'PHP', php, 'Philippines Peso'));
    currencyList.add(
        CurrencyItem('assets/images/hkd.png', 'HKD', hkd, 'Hong Kong Dollar'));
    currencyList.add(
        CurrencyItem('assets/images/myr.png', 'MYR', myr, 'Malaysian Ringgit'));
    currencyList.add(
        CurrencyItem('assets/images/brl.png', 'BRL', brl, 'Brazilian Real'));
    currencyList.add(
        CurrencyItem('assets/images/cad.png', 'CAD', cad, 'Canadian Dollar'));
    currencyList
        .add(CurrencyItem('assets/images/chf.png', 'CHF', chf, 'Swiss Franc'));
    currencyList
        .add(CurrencyItem('assets/images/czk.png', 'CZK', czk, 'Czech Koruna'));
    currencyList
        .add(CurrencyItem('assets/images/dkk.png', 'DKK', dkk, 'Danish Krone'));
    currencyList.add(
        CurrencyItem('assets/images/egp.png', 'EGP', egp, 'Egyptian Pound'));
    currencyList.add(
        CurrencyItem('assets/images/gbp.png', 'GBP', gbp, 'Pound Sterling'));
    currencyList.add(
        CurrencyItem('assets/images/aud.png', 'AUD', aud, 'Australian Dollar'));
    currencyList.add(
        CurrencyItem('assets/images/ils.png', 'ILS', ils, 'Israeli Shekel'));
    currencyList.add(
        CurrencyItem('assets/images/kes.png', 'KES', kes, 'Kenya Shilling'));
    currencyList.add(
        CurrencyItem('assets/images/khr.png', 'KHR', khr, 'Cambodian Riel'));
    currencyList.add(
        CurrencyItem('assets/images/kwd.png', 'KWD', kwd, 'Kuwaiti Dinar'));
    currencyList
        .add(CurrencyItem('assets/images/lak.png', 'LAK', lak, 'Lao Kip'));
    currencyList.add(
        CurrencyItem('assets/images/lkr.png', 'LKR', lkr, 'Sri Lankan Rupee'));
    currencyList.add(
        CurrencyItem('assets/images/nok.png', 'NOK', nok, 'Norwegian Kroner'));
    currencyList.add(
        CurrencyItem('assets/images/npr.png', 'NPR', npr, 'Nepalese Rupee'));
    currencyList.add(CurrencyItem(
        'assets/images/nzd.png', 'NZD', nzd, 'New Zealand Dollar'));
    currencyList.add(
        CurrencyItem('assets/images/pkr.png', 'PKR', pkr, 'Pakistani Rupee'));
    currencyList.add(
        CurrencyItem('assets/images/rsd.png', 'RSD', rsd, 'Serbian Dinar'));
    currencyList.add(
        CurrencyItem('assets/images/rub.png', 'RUB', rub, 'Russian Rouble'));
    currencyList.add(CurrencyItem(
        'assets/images/sar.png', 'SAR', sar, 'Saudi Arabian Riyal'));
    currencyList.add(
        CurrencyItem('assets/images/sek.png', 'SEK', sek, 'Swedish Krona'));
    currencyList.add(
        CurrencyItem('assets/images/zar.png', 'ZAR', zar, 'South Africa Rand'));

    return currencyList;
  }
}

class TempStore {
  static final _instance = TempStore._construct();

  factory TempStore() {
    return _instance;
  }

  TempStore._construct();

  List<CurrencyItem> currencyList = List();
}
