class CurrencyItem {
  String _countryFlag;
  String _currency;
  String _value;
  String _label;

  CurrencyItem(this._countryFlag, this._currency, this._value, this._label);

  String get label => _label;

  String get value => _value;

  String get currency => _currency;

  String get countryFlag => _countryFlag;
}
